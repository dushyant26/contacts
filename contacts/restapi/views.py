from os import stat
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from restapi.Serializers import ContactSerializer
from restapi.models import Contact
# Create your views here.

@api_view(['GET','POST'])
def ContactList(request):
    if request.method == 'GET':
        contacts = Contact.objects.all()
        serializer = ContactSerializer(contacts, many = True)
        return Response({'data':serializer.data})

    if request.method == 'POST':
        data = request.data

        if data.get('name') == None or data.get('email') == None or data.get('phone') == None:
            return Response({'msg':'Complete all the fields'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        contact = Contact(name = data['name'], email = data['email'], phone = data['phone'])
        contact.save()
        return Response({'msg':"Contact created successfully", 'data':ContactSerializer(contact).data}, status=status.HTTP_201_CREATED)

@api_view(['GET','PUT','PATCH','DELETE'])
def ContactUpdateList(request, id):

    try:
        contact = Contact.objects.get(id = id)
    except:
        return Response({'msg':'No entry with this id'}, status=status.HTTP_404_NOT_FOUND)

    data = request.data
    if request.method == 'GET':
        serializer = ContactSerializer(contact)
        return Response({'data':serializer.data})

    if request.method == 'PUT':
        if data.get('name') == None or data.get('email') == None or data.get('phone') == None:
            return Response({'msg':'Complete all the fields'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        contact.name = data.get('name')
        contact.email = data.get('email')
        contact.phone = data.get('phone')
        contact.save()
        return Response({'msg':"Contact Updated successfully", 'data':ContactSerializer(contact).data}, status=status.HTTP_202_ACCEPTED)

    if request.method == 'PATCH':

        if data.get('name') == None and data.get('email') == None and data.get('phone') == None:
            return Response({'msg':'Atleast one field required for update'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        contact.name = data.get('name',contact.name)
        contact.email = data.get('email',contact.email)
        contact.phone = data.get('phone',contact.phone)
        contact.save()
        return Response({'msg':"Contact Updated successfully", 'data':ContactSerializer(contact).data}, status=status.HTTP_202_ACCEPTED)

    if request.method == 'DELETE':
        contact.delete()
        return Response({'msg':"Contact Deleted successfully", 'data':ContactSerializer(contact).data}, status=status.HTTP_202_ACCEPTED)
    