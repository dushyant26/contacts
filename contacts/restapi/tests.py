import json
from urllib import response
from django.test import TestCase
from django.urls import reverse

from restapi.models import Contact

# Create your tests here.
class TestContact(TestCase):

    def setUp(self):
        contact = Contact(name = "Dushyant",email = "a@b",phone = "98989")
        contact.save()
        contact = Contact(name = "abc", email = "a@bc", phone = "98764")
        contact.save()

    def test_contact_get_all(self):
        response = self.client.get(reverse('contact-list'))
        data = json.loads(response.content)
        self.assertEquals(response.status_code,200)
        self.assertEquals(len(data.get('data')), 2) 

    def test_contact_get_with_id(self):
        response = self.client.get(reverse('contact-list-update', kwargs={'id':'2'}))
        self.assertEqual(response.status_code,200)
        data = json.loads(response.content)
        self.assertEquals(data.get('data'),{"id":2,"name" : "abc", "email" : "a@bc", "phone" : "98764"})

    def test_contact_get_with_wrong_id(self):
        response = self.client.get(reverse('contact-list-update', kwargs={'id':'7'}))
        self.assertEqual(response.status_code,404)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"No entry with this id")

    def test_contact_post_correct_input(self):
        response = self.client.post(reverse('contact-list'),{"name":"Dushyant Batra","email":"abc@gmail.com","phone":"9991656754"})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 201)
        self.assertEquals(data.get('msg'),"Contact created successfully")
        self.assertDictEqual(data.get('data'), {"id":3,"name":"Dushyant Batra","email":"abc@gmail.com","phone":"9991656754"})
        self.assertEqual(data.get('data').get('phone'),"9991656754")


    def test_contact_post_no_body(self):
        response = self.client.post(reverse('contact-list'))
        data = json.loads(response.content)
        self.assertEqual(response.status_code,406)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_post_empty_body(self):
        response = self.client.post(reverse('contact-list'),{})
        data = json.loads(response.content)
        self.assertEqual(response.status_code,406)
        self.assertEquals(data.get('msg'),"Complete all the fields")
        
    def test_contact_post_no_phone_attr(self):
        response = self.client.post(reverse('contact-list'),{"name":"Dushyant Batra","email":"abc@gmail.com"})
        data = json.loads(response.content)
        self.assertEqual(response.status_code,406)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_post_no_name_attr(self):
        response = self.client.post(reverse('contact-list'),{"email":"abc@gmail.com","phone":"9991656754"})
        data = json.loads(response.content)
        self.assertEqual(response.status_code,406)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_post_no_email_attr(self):
        response = self.client.post(reverse('contact-list'),{"name":"Dushyant Batra","phone":"9991656754"})
        data = json.loads(response.content)
        self.assertEqual(response.status_code,406)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_put_wrong_id(self):
        response = self.client.put(reverse('contact-list-update', kwargs={'id':'7'}),{'name':'Dushy'})
        self.assertEqual(response.status_code,404)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"No entry with this id")

    def test_contact_put_no_name_attr(self):
        response = self.client.put(reverse('contact-list-update', kwargs={'id':'2'}),{"email":"a*@g", "phone":"12345"},content_type="application/json")
        self.assertEqual(response.status_code,406)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_put_no_email_attr(self):
        response = self.client.put(reverse('contact-list-update', kwargs={'id':'2'}),{'name':'Dushyant Batra', 'phone':'12345'},content_type="application/json")
        self.assertEqual(response.status_code,406)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_put_no_phone_attr(self):
        response = self.client.put(reverse('contact-list-update', kwargs={'id':'2'}),{'name':'Dushyant Batra', 'email':'a*@g'},content_type="application/json")
        self.assertEqual(response.status_code,406)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Complete all the fields")

    def test_contact_put_empty_body(self):
        response = self.client.put(reverse('contact-list-update', kwargs={'id':'2'}),{},content_type="application/json")
        self.assertEqual(response.status_code,406)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Complete all the fields")


    def test_contact_put_correct_input(self):
        response = self.client.put(reverse('contact-list-update', kwargs={'id':'2'}),{'name':'Dushyant Batra', 'email':'a*@g', 'phone':'12345'},content_type="application/json")
        self.assertEqual(response.status_code,202)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Contact Updated successfully")
        self.assertDictEqual(data.get('data'), {'id':2,'name':'Dushyant Batra', 'email':'a*@g', 'phone':'12345'})

    def test_contact_put_without_id(self):
        response = self.client.put(reverse('contact-list'),{'name':'Dushyant Batra', 'email':'a*@g', 'phone':'12345'})
        self.assertEqual(response.status_code,405)

    def test_contact_patch_wrong_id(self):
        response = self.client.patch(reverse('contact-list-update', kwargs={'id':'7'}))
        self.assertEqual(response.status_code,404)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"No entry with this id")

    def test_contact_patch_empty_body(self):
        response = self.client.patch(reverse('contact-list-update', kwargs={'id':'1'}),{},content_type="application/json")
        self.assertEqual(response.status_code,406)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Atleast one field required for update")


    def test_contact_patch_correct_input(self):
        response = self.client.patch(reverse('contact-list-update', kwargs={'id':'1'}),{'name':'Dushyant Batra'},content_type="application/json")
        self.assertEqual(response.status_code,202)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Contact Updated successfully")
        self.assertDictEqual(data.get('data'), {'id':1,'name':'Dushyant Batra', "email" : "a@b","phone": "98989"})

    def test_contact_patch_without_id(self):
        response = self.client.patch(reverse('contact-list'),{'name':'Dushyant Batra'})
        self.assertEqual(response.status_code,405)

    def test_contact_delete_wrong_id(self):
        response = self.client.delete(reverse('contact-list-update', kwargs={'id':'7'}))
        self.assertEqual(response.status_code,404)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"No entry with this id")

    def test_contact_delete_correct_input(self):
        response = self.client.delete(reverse('contact-list-update', kwargs={'id':'2'}))
        self.assertEqual(response.status_code,202)
        data = json.loads(response.content)
        self.assertEquals(data.get('msg'),"Contact Deleted successfully")

    def test_contact_delete_without_id(self):
        response = self.client.delete(reverse('contact-list'))
        self.assertEqual(response.status_code,405)


   