from django.urls import path
from . import views

urlpatterns = [
    path('contact/',views.ContactList, name = 'contact-list'),
    path('contact/<int:id>/',views.ContactUpdateList, name = 'contact-list-update')
]
